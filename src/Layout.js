
import React, {Component} from 'react';
import layout_styles from './layout.css';
import Product from './Product.js';
import cinnamon from './assets/img/cinnamon.jpeg';
import coffee from './assets/img/coffee.jpg';
import flour from './assets/img/flour.jpg';
import pepper from './assets/img/pepper.jpg';
import red_pepper from './assets/img/red_pepper.jpeg';
import shopping_bag from './assets/img/shopping_bag.svg';
import sugar from './assets/img/sugar.jpeg';
import details from './product_details.json';
import InsideBag from './InsideBag.js';




class Layout extends Component {


state={
"pepper":0,
"flour":0,
"coffee":0,
"cinnamon":0,
"sugar":0,
"red_pepper":0,
"total":0,
"clicked":false,
"cancel_order":false,
"rpepper_count":0
}


forTotal=(suma,ajdik)=>{

   this.setState({total:this.state.pepper+this.state.flour+
      this.state.coffee+this.state.cinnamon+this.state.sugar+
      this.state.red_pepper});
   
      
}


handleClick=()=>{

this.setState({clicked:true});

}


closeHandleClick=()=>{
this.setState({clicked:false});

}


cancelHandleClick=()=>{

this.setState({cancel_order:true});


}

passingHandler=(sum,specific_product_id)=>{


   if(specific_product_id===details.item_id.pepper_id){
   this.setState({pepper:sum},()=>{this.forTotal(sum,specific_product_id)});

      
}  else if(specific_product_id===details.item_id.flour_id){

   this.setState({flour:sum},()=>{this.forTotal(sum,specific_product_id)}); 

  
}  else if(specific_product_id===details.item_id.coffee_id){
     this.setState({coffee:sum},()=>{this.forTotal(sum,specific_product_id)});

}  else if(specific_product_id===details.item_id.cinnamon_id){
   this.setState({cinnamon:sum},()=>{this.forTotal(sum,specific_product_id)});

}  else if(specific_product_id===details.item_id.sugar_id){
   this.setState({sugar:sum},()=>{this.forTotal(sum,specific_product_id)});

}  else if(specific_product_id===details.item_id.red_pepper_id){
      this.setState({red_pepper:sum},()=>{this.forTotal(sum,specific_product_id)});
   
}

}

summaryHandler=(type,itm)=>{


 if(type==="pepper"&& itm>0){
      return ((Math.round(this.state.pepper*100)/100)) + details.item_price.currency +"  for pepper";
   }
 if (type==="flour"&& itm>0){
       return ((Math.round(this.state.flour*100)/100)) + details.item_price.currency + "  for flour";
  
   } if(type==="coffee" && itm>0){
      return ((Math.round(this.state.coffee*100)/100)) + details.item_price.currency +"  for coffee";
   }  if(type==="cinnamon" && itm>0){
      return ((Math.round(this.state.cinnamon*100)/100))+ details.item_price.currency +"  for cinnamon";
   }  if (type==="sugar"&& itm>0){
      return ((Math.round(this.state.sugar*100)/100)) + details.item_price.currency +"  for sugar";
   }  if (type==="red pepper"&& itm>0){
            if(this.state.rpepper_count<2){
      return ((Math.round(this.state.red_pepper*100)/100)) + details.item_price.currency +"  for red pepper";     
   } else if (this.state.rpepper_count>=2){
              return  ((Math.round(this.state.red_pepper*100)/100))+ details.item_price.currency +"  for red peppers";
   }      
   }
}

numHandler=(counter_state)=>{

   this.setState({rpepper_count:counter_state});
}



totalBeautyHandler=()=>{
   


   if (this.state.total<=0){
      return "SUM";
   } else if(this.state.total>0){
      return ((Math.round(this.state.total*100)/100)+details.item_price.currency) ;    
   }

}




   render(){

   


if (this.state.cancel_order){return <Layout />};



      return (

    
         <div className="container-fluid" id="content">

         <div className="row">

         		     <div className="wrapper">
                     <img  alt="shop"id="shop" src={shopping_bag} role="button" onClick={this.handleClick}/>              
                 <h6 id="sum">{this.totalBeautyHandler()}</h6>
         		</div>
         </div>
         

         <InsideBag inorvisible={this.state.clicked} closing={this.closeHandleClick} cancelling={this.cancelHandleClick}
        
          pepper_summary={this.summaryHandler("pepper",(Math.round(this.state.pepper*100)/100))}
          flour_summary ={this.summaryHandler("flour",(Math.round(this.state.flour*100)/100))}
         coffee_summary={this.summaryHandler("coffee",(Math.round(this.state.coffee*100)/100))}
         cinnamon_summary={this.summaryHandler("cinnamon",(Math.round(this.state.cinnamon*100)/100))}
         sugar_summary={this.summaryHandler("sugar",(Math.round(this.state.sugar*100)/100))}
         red_pepper_summary={this.summaryHandler("red pepper",(Math.round(this.state.red_pepper*100)/100))}
          />


            <div className="row">

               <div className="col-sm-4 col-md-4 col-lg-4">
            <Product product_picture={pepper} item_name={details.item.pepper}
                price={details.item_price.pepper_price}
               currency={details.item_price.currency}
               idk={details.item_id.pepper_id}
               pass={this.passingHandler}
               typenum={this.numHandler}
               />
               </div>

               <div className="col-sm-4 col-md-4 col-lg-4">
               <Product product_picture={flour} item_name={details.item.flour}
               price={details.item_price.flour_price}
               currency={details.item_price.currency}
               idk={details.item_id.flour_id}
               pass={this.passingHandler}
               typenum={this.numHandler}
               /> 
               </div>


                  <div className="col-sm-4 col-md-4 col-lg-4">
                  <Product product_picture={coffee} item_name={details.item.coffee} 
                  price={details.item_price.coffee_price}
               currency={details.item_price.currency}
               idk={details.item_id.coffee_id}
               pass={this.passingHandler}
               typenum={this.numHandler}
               />
                  </div>
               </div>


               <div className="row">
                  <div className="col-sm-4 col-md-4 col-lg-4">
                  <Product product_picture={cinnamon} item_name={details.item.cinnamon} 
                  price={details.item_price.cinnamon_price}
               currency={details.item_price.currency}
               idk={details.item_id.cinnamon_id}
               pass={this.passingHandler}
               typenum={this.numHandler}
               
               />
                  </div>
                  <div className="col-sm-4 col-md-4 col-lg-4">
                  <Product product_picture={sugar} item_name={details.item.sugar} 
                  price={details.item_price.sugar_price}
               currency={details.item_price.currency} 
               idk={details.item_id.sugar_id}
               pass={this.passingHandler}
               typenum={this.numHandler}
               />
                  </div>
                  <div className="col-sm-4 col-md-4 -col-lg-4">
                  
                  <Product product_picture={red_pepper} item_name={details.item.red_pepper} 
                  price={details.item_price.red_pepper_price}
               currency={details.item_price.currency}
               idk={details.item_id.red_pepper_id}
               pass={this.passingHandler} 
               typenum={this.numHandler}
             
               />
                  </div>
               </div>

               <h1>Icon made by CC 3.0 BY from www.flaticon.com</h1><br></br>
               

         </div>
       
      );
  };
};

export default Layout;