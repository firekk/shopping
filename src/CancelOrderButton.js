import React from 'react';
import cancel from './assets/img/cancel.svg';



const CancelOrderButton =(props)=>{


return(

<img alt="tag_btn" src={cancel} type="button" onClick={props.tocancel} className='tag_buttons' id='tag_button_cancel'/>

);

};

export default CancelOrderButton;