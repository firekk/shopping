
import React, {Component} from 'react';
import price_tag from './assets/img/price_tag.png';
/*import { parse } from 'url';
import details from './product_details.json';
*/




class Product extends React.Component{


 

   state={
      "counter":0,
      "price_sum":0
      
      }
   



         addItemHandler=()=>{
          
            if(this.state.counter>= 999){return};   
        
          
            let new_counter_value = this.state.counter+1;
          
         this.setState({counter:new_counter_value,
               price_sum :new_counter_value*this.props.price},
               ()=>{
               this.props.pass(this.state.price_sum,this.props.idk);
              this.props.typenum(this.state.counter);           
               }
               );
         }
      

         subtractItemHandler=()=>{

            if(this.state.counter>0){
               let new_counter_value = this.state.counter-1;
            this.setState({counter:new_counter_value,price_sum:new_counter_value*this.props.price},
               ()=>{
                 this.props.pass(this.state.price_sum,this.props.idk);
             
               }
            );          
            } 
         }
      



testHandler=(event)=>{

   let note=parseFloat(event.target.value);
   
   if(isNaN(note)||note>999){

      return;
   }

  this.setState({counter:note,price_sum:this.props.price*note},
   ()=>{
  this.props.pass(this.state.price_sum,this.props.idk);
  this.props.typenum(this.state.counter);
});

};

   
render(){


return(

<div>
<img className="product" src={this.props.product_picture} alt="product"/>

   <div className="special_container">
      <img  alt="price_tag" className="price_t" src={price_tag}/>
      <p>{this.props.item_name}</p>

      <div className="amount_box">
         <button id="down" className="qty" onClick={this.subtractItemHandler}>-</button>

         <input className="amount"id={this.props.idk} 
          onChange={this.testHandler} maxLength="3"
          value={this.state.counter} 

          />
         
         <button id="up" className="qty" onClick={this.addItemHandler}>+</button>
      </div>
      <h6 className="price">{this.state.counter}</h6>
      <h6 className="price_visual" id="p1v">{this.props.price}{this.props.currency}
      </h6>
   </div>

</div>

);
};
};


export default Product;
