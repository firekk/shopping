import React,{Component} from 'react';
import details from './product_details.json';
import shopping_bag from './assets/img/shopping_bag.svg';
import Product from './Product.js';
import InsideBagStyles from './InsideBag.css';
import ClosingButton from './ClosingButton';
import CancelOrderButton from './CancelOrderButton';
import ProceedButton from './ProceedButton';



class InsideBag extends Component{


render(){

if(!this.props.inorvisible){
return null;

}



return(


<div className="bag_container">
<p id="p_inside_bag"> You are going to pay:</p>

<ul>
<li>{this.props.pepper_summary}</li>
<li>{this.props.flour_summary}</li>
<li>{this.props.coffee_summary}</li>
<li>{this.props.cinnamon_summary}</li>
<li>{this.props.sugar_summary}</li>
<li>{this.props.red_pepper_summary}</li>
</ul>


  <ClosingButton toclose={this.props.closing}/>
  <CancelOrderButton tocancel={this.props.cancelling}/>
  <ProceedButton />



</div>

);

};
};

export default InsideBag;

