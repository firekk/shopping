import React from 'react';
import proceed from './assets/img/proceed.svg';
import {Link} from 'react-router-dom';


const ProceedButton=()=>{

return(
<Link to='/greeting'>
<img  alt="proceed"src={proceed} type="button" className='tag_buttons' id='tag_button_proceed'/>
</Link>
)

};

export default ProceedButton;