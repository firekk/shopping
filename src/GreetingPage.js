
import React from 'react';
import greeting_styles from './GreetingPage.css';
import greeting_img from'./assets/img/greeting.jpeg';


const GreetingPage=()=>{


return(
  


<div className="container-fluid" id="backdrop">
<img  alt="thank_you_page" id="greet" src={greeting_img} /> 
<h1 id="grt_caption">Thank you for shopping with us, see you soon !</h1>

</div>
);

}




export default GreetingPage;